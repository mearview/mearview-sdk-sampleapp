precision mediump float;
uniform sampler2D textureCam;
varying vec2 texCoord;

void main(void) {
    gl_FragColor = texture2D(textureCam, texCoord);
}