//
//  ViewController.swift
//  MVTDemo
//
//  Copyright © 2019 MearView. All rights reserved.
//

import UIKit
import SceneKit

@objc class ViewController: UIViewController, MatrixDelegate {
    
    // Members outlets
    @IBOutlet var scnView: SCNView!
    var viewController: MVViewController!
    
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    var curDaeFilename = String()  
    
    var matrixModel = SCNMatrix4Identity
    var matrixView = SCNMatrix4Identity
    var matrixProj = SCNMatrix4Identity
    var matrixToHideScnView: SCNMatrix4!
    
    var emptyScene: SCNScene!
    var material: SCNMaterial!
    
    var shoeScene: SCNScene!
    var cameraNode: SCNNode!
    
    var shoeNode: SCNNode!
    var legNode: SCNNode!
    
    var isOptFlow: Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Create sub view controller
        viewController = MVViewController()
        viewController.delegate = self
        self.addChild(viewController)
        self.view.addSubview(viewController.view)
        viewController.didMove(toParent: self)

        self.view.bringSubviewToFront(scnView)
        self.view.bringSubviewToFront(resetBtn)
        self.view.bringSubviewToFront(startBtn)
        resetBtn.addTarget(self, action: #selector(resetBtnAction), for: UIControl.Event.touchUpInside)
        startBtn.addTarget(self, action: #selector(startBtnAction), for: UIControl.Event.touchUpInside)
        
        setIsOpticalFlow()
        
        scnView.isPlaying = true
        scnView.isOpaque = false
        scnView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        
        // Tap through
        self.scnView.isUserInteractionEnabled = false
        
        self.emptyScene = SCNScene()
        
        createScene()
        calcModelMatrix()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewController.view.frame = scnView.frame
    }
    
    private func setIsOpticalFlow() {

    }
    
    @objc func resetBtnAction() {
        viewController.resetButtonPressed()
    }
    
    @objc func startBtnAction() {
        viewController.startButtonPressed()
    }
    
    func calcModelMatrix() {
        
        
        let ShoeRotateX: Float = 25
        
        var v1 = SCNVector3(x:0, y:0, z:0)
        var v2 = SCNVector3(x:0, y:0, z:0)
        
        shoeScene.rootNode.childNodes.first!.__getBoundingBoxMin(&v1, max: &v2)
        
        let intendedWidth: Float = 15//11
        let modelOriginWidth = v2.x - v1.x  // original width of the shoe model (in Blender tool)
        
        let scale: Float = intendedWidth / modelOriginWidth
        
        matrixModel = SCNMatrix4Identity
        matrixModel = SCNMatrix4Scale(matrixModel, scale, scale, scale)
        matrixModel = SCNMatrix4Rotate(matrixModel,
                                      GLKMathDegreesToRadians(180), 1.0, 0.0, 0.0)
        matrixModel = SCNMatrix4Rotate(matrixModel,
                                      GLKMathDegreesToRadians(270), 0.0, 0.0, 1.0)//ios13
//                                      GLKMathDegreesToRadians(90), 0.0, 0.0, 1.0)//ios12
//        matrixModel = SCNMatrix4Rotate(matrixModel,
//                                       GLKMathDegreesToRadians(90), 0.0, 1.0, 0.0)
        
        // Move model to right position!!!
        // X IS Y, Y IS X
        // But still Z is messed up. Going IN is positive now.
        matrixModel = SCNMatrix4Translate(matrixModel, -3, 0, 10)
        
        // The OpenCV code is messed up... X is now Y
        matrixModel = SCNMatrix4Rotate(matrixModel,
                                       GLKMathDegreesToRadians(ShoeRotateX), 0.0, 1.0, 0.0)
        
        matrixToHideScnView = SCNMatrix4Translate(SCNMatrix4Identity, 1000, 1000, 1000);
    }

    
    func addCameraNode(_ scene: SCNScene!) {
        
        cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0.0, y: 0.0, z: 0.0)
        // Rotate 90 @ X Axis. DK why, just been told openCV gives such values.
        var camRot = SCNMatrix4Identity
        camRot = SCNMatrix4Rotate(camRot, Float(M_PI), 1, 0, 0)
        cameraNode.transform = camRot
        
        scene.rootNode.addChildNode(cameraNode)
        
    }
    
    func addAmbientLightNode(_ scene: SCNScene!) {
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = SCNLight.LightType.ambient
        ambientLightNode.light!.color = UIColor(red: 0.60, green: 0.60, blue: 0.60, alpha: 1)
        
        scene.rootNode.addChildNode(ambientLightNode)
        
    }
    
    func setShaderModifier(_ geo: SCNGeometry) {
        
        var vertexShader: String
        var fragShader: String
        self.material = SCNMaterial()
        do {
            try vertexShader = String(contentsOfFile: Bundle.main.path(forResource: "myvertex", ofType: "glsl")!)
            try fragShader = String(contentsOfFile: Bundle.main.path(forResource: "myfrag", ofType: "glsl")!)
            let scrnDim = CGPoint(x: scnView.frame.width, y: scnView.frame.height)
            
            //program
            // 0. https://developer.apple.com/reference/scenekit/scnprogram Is it possible to set GL_BLEND in handleBinding(ofSymbol:handler:) ??? If not try to pass a texture using opengl code
            // 1. Will the program work without a vertex shader?
            // 2. program delegate = self may cause runtime error because self is a swift class?
            let program = SCNProgram()
            program.vertexShader = vertexShader
            program.fragmentShader = fragShader
            
            program.setSemantic(SCNGeometrySource.Semantic.vertex.rawValue, forSymbol: "position", options: nil)
            program.setSemantic(SCNGeometrySource.Semantic.texcoord.rawValue, forSymbol: "textureCoordinate", options: nil)
            program.setSemantic(SCNModelViewProjectionTransform, forSymbol: "modelViewProjection", options: nil)
            
            material.program = program
            
            material.setValue(NSValue(cgPoint: scrnDim), forKey: "scrnDim")//CGPoint is just a structure so wrap it with NSValue so that is converted to Object.
            
            material.handleBinding(ofSymbol: "textureCam", handler: {(programId: UInt32, location: UInt32, renderedNode: SCNNode?, renderer: SCNRenderer) -> Void in
                glEnable(GLenum(GL_BLEND))
                glBlendFunc(GLenum(GL_SRC_ALPHA), GLenum(GL_ONE_MINUS_SRC_ALPHA))
                glBlendEquation(GLenum(GL_FUNC_REVERSE_SUBTRACT))
                
                })
            
            material.handleUnbinding(ofSymbol: "textureCam", handler: {(programId: UInt32, location: UInt32, renderedNode: SCNNode?, renderer: SCNRenderer) in
                glBlendEquation(GLenum(GL_FUNC_ADD))
                glDisable(GLenum(GL_BLEND))
                })
            
            
            geo.materials = [material]
            
        } catch {
            NSLog("Load shader file error")
        }
    }
    
    func program(_ program: SCNProgram, handleError error: NSError) {
        NSLog("%@", error)
    }
    
    func createMatrixToBringViewOutOfBound() -> SCNMatrix4 {
        var matrix = SCNMatrix4Identity
        matrix = SCNMatrix4Rotate(matrix, GLKMathDegreesToRadians(90), 0.0, 1.0, 0.0)//rotate along y
        matrix = SCNMatrix4Translate(matrix, 0.0, 0.0, 1000)//move out 1000
        return matrix
    }
    
    func createScene() {
        shoeScene = SCNScene(named: "shoe.dae")!
        shoeNode = shoeScene.rootNode.childNodes[0]
        let legScene = SCNScene(named: "foot.dae")!
        let leg = legScene.rootNode.childNode(withName: "FIGURE_MAN", recursively:true)!
        legNode = leg
        setShaderModifier(leg.geometry!)
        
        shoeScene.rootNode.addChildNode(leg)
        addCameraNode(shoeScene)
        addAmbientLightNode(shoeScene)
        
        scnView.scene = self.shoeScene
        scnView.showsStatistics = false
        scnView.allowsCameraControl = false
        scnView.autoenablesDefaultLighting = true
    }
    
    func updateMVMatrix(_ m: SCNMatrix4)
    {
        let mt = SCNMatrix4Mult(matrixModel, m)
        shoeNode.transform = mt
        legNode.transform = mt
    }
    
    func updateMVInvisible()
    {
        shoeNode.transform = matrixToHideScnView
        legNode.transform = matrixToHideScnView
    }
}
