//
//  MVViewController.h
//  MearViewTrackingSDK
//
//  Copyright © 2019 MearView. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import <UIKit/UIKit.h>

@protocol MatrixDelegate <NSObject>
- (void)updateMVMatrix:(SCNMatrix4)m;
- (void)updateMVInvisible;
@end

@class TrackingViewController;

@interface MVViewController : UIViewController

@property (nonatomic, assign) id<MatrixDelegate> delegate;
- (void)resetButtonPressed;
- (void)startButtonPressed;

@end


